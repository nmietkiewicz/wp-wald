<?php
/*
Template Name: Miejsce
*/
get_header(); ?>
<div id="page-place-single" class="content content-single">
	<div class="page-place-single-overlay">
		<div class="wrapper">
			<main id="main">
			<?php query_posts( array(
				'post_type'      => 'page',
				'posts_per_page' => -1,
				'page_id'    => $post->ID,
				'post_parent'    => $post->parentID,
				'order'          => 'DESC',
				'orderby'        => 'date'
			)); ?>
				<?php
				while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/content', 'place-page' );
				endwhile;
				?>

			</main>
		</div>
	</div>
</div>
<?php
get_footer();
