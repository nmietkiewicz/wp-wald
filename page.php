<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */

get_header(); ?>
<div id="content" class="content-page">
	<div class="wrapper">
		<main id="main">
		<?php query_posts( array(
			'post_type'      => 'page',
			'posts_per_page' => -1,
			'page_id'    => $post->ID,
			'post_parent'    => $post->parentID,
			'order'          => 'DESC',
			'orderby'        => 'date'
		)); ?>
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.


			?>

		</main>
	</div>
</div>
<?php
get_footer();
