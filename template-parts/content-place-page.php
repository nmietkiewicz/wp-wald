<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="navigation">
		<?php
			$parent = $post->post_parent; $pagelist = get_pages('post_type=page&sort_column=menu_order&sort_order=desc&child_of='.$parent);
			$pages = array(); foreach ($pagelist as $page) {$pages[] += $page->ID;}
			$current = array_search($post->ID, $pages);
			$prevID = $pages[$current-1];
			$nextID = $pages[$current+1];
		?>
		<span>
		<?php if (!empty($nextID)) { ?>
		<a href="<?php echo get_permalink($nextID); ?>" title="<?php echo get_the_title($nextID); ?>">&laquo;</a>
		<?php } ?>
		</span>

		<span>
		<?php if (!empty($prevID)) { ?>
		<a href="<?php echo get_permalink($prevID); ?>" title="<?php echo get_the_title($prevID); ?>">&raquo;</a>
		<?php } ?>
		</span>
	</div>

	<section class="section-top">
		<div class="col-left entry">
			<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>

			</header>
			<div class="entry-content">
				<?php the_content(); ?>
				<?php // edit_post_link(); ?>
			</div>

		</div>

		<?php if(get_field('wpmap')): ?>
		<div class="col-right">
			<div class="entry-map">
				<?php echo do_shortcode( '[put_wpgm id="'.get_field('wpmap').'"]' ); ?>
				<?php if(is_user_logged_in()): ?>
					<a href="/wp-admin/admin.php?page=wpgmp_form_map&doaction=edit&map_id=<?php echo get_field('photoblocks'); ?>">edit map</a>
				<?php endif; ?>
			</div>
		</div>
		<?php endif; ?>
	</section>
	<!-- <div>
		<h4>Powiązane historie</h4>
		<ul>
			<li>dfds</li>
		</ul>
	</div> -->

	<section class="section-bottom">
		<?php if(get_field('photoblocks')): ?>
			<?php echo do_shortcode( '[photoblocks id='.get_field('photoblocks').']' ); ?>
			<?php if(is_user_logged_in()): ?>
				<a href="/wp-admin/admin.php?page=photoblocks-edit&id=<?php echo get_field('photoblocks'); ?>">edit gallery</a>
			<?php endif; ?>
		<?php endif; ?>
	</section><!-- .entry-footer -->

	<footer class="entry-footer">
		<?php echo get_field('cities') ? get_field('cities').', ':''; ?><?php the_date('F Y', '<span>', '</span>'); ?>
	</footer><!-- .entry-footer -->

	<section>
		<?php
	// 	if ( comments_open() || get_comments_number() ) :
 //     comments_template();
 // endif;
  ?>
	</section>
</article><!-- #post-<?php the_ID(); ?> -->
