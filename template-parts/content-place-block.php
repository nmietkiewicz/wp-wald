<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */

?>

<article id="post-<?php the_ID(); ?>" class="box entry-holder photo-holder <?php echo get_post_class(); ?>">
	<?php _s_post_thumbnail(); ?>
	<?php echo _w_get_trip_title(get_the_id()); ?>

</article><!-- #post-<?php the_ID(); ?> -->
