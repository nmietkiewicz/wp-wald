<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */

?>

<article id="post-<?php the_ID(); ?>" class="box entry-holder <?php echo get_post_class(); ?>">
	<div class="left-col">
		<?php _s_post_thumbnail(); ?>
	</div>
	<div class="right-col">
		<header class="entry-header">
			<h2 class="entry-title">
				<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
					<?php the_title(); ?>
				</a>
			</h2>
			<div class="entry-meta">
				<?php the_date(); ?> / <?php echo _w_get_trip_title(get_the_id()); ?>
			</div>
		<div class="entry-text">
			<?php the_excerpt(); ?>
		</div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
