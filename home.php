<?php get_header(); ?>

<div id="page-stories-list" class="content content-list">
	<div class="wrapper">
		<main id="main">
			<header class="entry-header" id="page-header">
				<h1 class="entry-title">Historie</h1>
			</header>

		<?php
		if ( have_posts() ) :

			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content', 'post' );
			endwhile;

			the_posts_navigation();

		else :
			get_template_part( 'template-parts/content', 'none' );
		endif;
		?>

		</main>
	</div>
</div>

<?php get_footer();
