<?php
/*
Template Name: Lista Miejsc
*/
get_header(); ?>

<div id="page-places-list" class="content content-list">
	<div class="wrapper">
		<main id="main" class="threecols">
			<?php while ( have_posts() ) : the_post(); ?>
        <?php
          $args = array(
            'post_type'      => 'page',
            'posts_per_page' => -1,
            'post_parent'    => $post->ID,
            'order'          => 'ASC',
            'orderby'        => 'menu_order'
         );
         $parent = new WP_Query( $args );
        ?>

        <?php
    		if ( $parent->have_posts() ) :
    			while ( $parent->have_posts() ) : $parent->the_post();
    				get_template_part( 'template-parts/content', 'place-block' );
    			endwhile;
    			the_posts_navigation();
    		else :
    			get_template_part( 'template-parts/content', 'none' );
    		endif;
        ?>



        <?php wp_reset_query(); ?>



      <?php endwhile; ?>

		</main>
	</div>
</div>
<?php
get_footer();
